#include <linux/init.h>
#include <linux/module.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/device.h>
#include <linux/types.h>
#include <linux/uaccess.h>
#include <linux/workqueue.h>
 
 
MODULE_LICENSE("GPL");

static int driver_open(struct inode *geraetedatei, struct file *instanz);
static int driver_close(struct inode *geraetedatei, struct file *instanz);
static ssize_t driver_read(struct file *instanz,char *user,size_t count, loff_t *offset);
static ssize_t driver_write(struct file *instanz, const char *user, ssize_t count, loff_t *offs);

static dev_t template_dev_number;
static struct cdev *driver_object;
static struct file_operations fops = {
	.open = driver_open,
	.release = driver_close,
	.read = driver_read,
	.write=driver_write
};
static struct class * template_class;

static atomic_t write_count;
static char workqueue_str[] = "Hello World!";

//WQ
static struct workqueue_struct *wq;

static void workqueue_func(void *data)
{
	pr_debug("workqueue_func... \n");
	return;
}

static DECLARE_WORK(work_obj, workqueue_func);

static int driver_open(struct inode *geraetedatei, struct file *instanz)
{
	printk(KERN_DEBUG "workqueue wird geoeffnet...");
	if(instanz->f_flags&O_RDWR || instanz->f_flags&O_WRONLY)
	{
		if(atomic_inc_and_test(&write_count) == 0) {
			printk(KERN_DEBUG "Sry Treiber gesperrt!");
			atomic_dec(&write_count);
			return -EBUSY;
		}
	}
	printk(KERN_DEBUG "Syso Treiber erfolgreich geoeffnet");
	return 0;
}

static int driver_close(struct inode *geraetedatei, struct file *instanz)
{
	if(instanz->f_flags&O_RDWR || instanz->f_flags&O_WRONLY)
	{
		printk(KERN_DEBUG "Syso Treiber wieder freigegeben!");
		atomic_dec(&write_count);
	}
	printk(KERN_DEBUG "workqueue closed");
	return 0;
}

static ssize_t driver_read(struct file *instanz,char *user,size_t count, loff_t *offset)
{
	unsigned long not_copied_byte, to_copy;
	to_copy = min(count, strlen(workqueue_str)+1);
	//printk(KERN_DEBUG "workqueue read aufruf");
	not_copied_byte = copy_to_user(user, workqueue_str, to_copy); 
	return to_copy-not_copied_byte;
}

static ssize_t driver_write(struct file *instanz, const char *user, ssize_t count, loff_t *offs)
{
	printk("workqueue write aufruf");
	printk("count: %d", count);
	return count;
}

static int __init ModInit(void)
{
	atomic_set(&write_count, -1);
        printk(KERN_ALERT "Hello, world\n");
	if(alloc_chrdev_region(&template_dev_number,0,1,"workqueue_treiber")<0)
	{
		printk(KERN_ALERT "Error reserving device number");
		return -EIO;
	}
	driver_object = cdev_alloc();	
	if (driver_object ==NULL)
	{
		printk(KERN_ALERT "Error adding driver!");
		goto free_device_nmber;
	}
	driver_object->owner = THIS_MODULE;
	driver_object->ops = &fops;
	if (cdev_add(driver_object,template_dev_number,1))
	{
		printk(KERN_ALERT "Error registering driver!");
		goto free_cdev;
	}

	//Init  WQ
	//
	wq = create_singlethread_workqueue("DrvrSmpl");	
	if(queue_work(wq,&work_obj))
		pr_debug("QUeuework succesful...\n");
	else
		pr_debug("Queuework not succesful ... \n");
		
	//Eintrag im Sysfs
	template_class = class_create( THIS_MODULE, "workqueue_class");
	device_create(template_class,NULL,template_dev_number,NULL,"%s", "workqueue_device");
	printk(KERN_ALERT "workqueue wurde angemeldet mit Major %i und minor %i", MAJOR(template_dev_number), MINOR(template_dev_number));
        return 0;

	free_cdev:
		kobject_put( &driver_object->kobj);
	free_device_nmber:
		unregister_chrdev_region(template_dev_number,1);
		return -EIO;
}
 
static void __exit ModExit(void)
{
	device_destroy(template_class,template_dev_number);
	class_destroy(template_class);
	cdev_del(driver_object);
	unregister_chrdev_region(template_dev_number,1);
        printk(KERN_ALERT "Goodbye, cruel world\n");
	//WQ DESTROY
	if(wq)
	{
		printk("Destroy WQ\n");
		destroy_workqueue(wq);
	}
	return;
}
 
module_init(ModInit);
module_exit(ModExit);
