#################################################################
# 
# syso3
#
#################################################################
SYSO3_VERSION = 1.0
SYSO3_SOURCE = syso3-$(SYSO3_VERSION).tar.gz
SYSO3_SITE = 
SYSO3_INSTALL_STAGING = NO
SYSO3_DIR = $(BUILD_DIR)/syso3-$(SYSO3_VERSION)

define SYSO3_EXTRACT_CMDS
	tar -C $(SYSO3_DIR)/$(TAR_OPTIONS) $(DL_DIR)/$(SYSO3_SOURCE)
endef

define SYSO3_BUILD_CMDS
	$(MAKE) CC="$(TARGET_CC)" LD="$(TARGET_LD)" -C $(@D) all
endef

define SYSO3_INSTALL_TARGET_CMDS
	cp $(SYSO3_DIR)/startc $(TARGET_DIR)/bin
	cp $(SYSO3_DIR)/startc_static $(TARGET_DIR)/bin
endef


$(eval $(generic-package))
