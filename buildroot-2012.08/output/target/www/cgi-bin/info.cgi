#!/bin/sh

echo Content-type: text/html
echo
echo "<html>"
echo "<head>"
echo "<title>Systemsoftware Versuch2 Soest Holzhauer</title>"
echo "</head>"
echo "<body>"
echo "Embedded Linux fuer Versuch2 Systemsoftware-ARM Architektur"
echo "<pre>"
echo "Operatingsystem:"
cat /proc/version
echo ""
echo ""
echo "Cpu Info:"
cat /proc/cpuinfo
echo ""
echo ""
echo "Uptime:"
uptime
echo ""
echo ""
echo "Network Info:"
ifconfig eth0
echo ""
echo ""
echo "</pre>"
echo "</body>"
echo "</html>"
