#include <stdio.h>
#include <fcntl.h>
#include <stdarg.h>
#include <unistd.h>

int main(int argc, char * args[])
{
	int n;
	int fd_ro1;
	char buf[200];
	//char *s = "hi!";

	if((fd_ro1 = open("/dev/hello_device", O_RDONLY)) < 0) {
		printf("fd_ro1: konnte Datei nicht zum Lesen oeffnen\n");
	}else{
		printf("fd_ro1: Datei zusaetzlich zum lesen geoeffnet\n");
	}

	n = read(fd_ro1, &buf, 20);
	printf("Read %i Bytes\n", n);
	return 0;
}
