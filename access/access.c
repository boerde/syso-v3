#include <stdio.h>
#include <fcntl.h>
#include <stdarg.h>
#include <unistd.h>

int main(int argc, char * args[])
{
	int n;
	int fd_wr1, fd_wr2, fd_ro1, fd_ro2;
	char buf[200];
	//char *s = "hi!";

	if((fd_wr1 = open("/dev/openclose_atomic_device", O_RDWR)) < 0) {
		printf("fd_wr1: konnte Datei nicht oeffnen\n");
	}else{
		printf("fd_wr1: Datei zum Lesen und Schreiben geöffnet\n");
	}

	if((fd_ro1 = open("/dev/openclose_atomic_device", O_RDONLY)) < 0) {
		printf("fd_ro1: konnte Datei nicht zum Lesen oeffnen\n");
	}else{
		printf("fd_ro1: Datei zusaetzlich zum lesen geoeffnet\n");
	}

	if((fd_wr2 = open("/dev/openclose_atomic_device", O_RDWR)) < 0) {
		printf("fd_wr2: konnte Datei nicht oeffnen\n");
	}
	else {
		printf("fd_wr2: Datei zum 2. mal zum lesen geöffnet ERROR\n");
	}

	n = read(fd_wr1, &buf, 200);
	printf("Read %i Bytes\n", n);
	n = write(fd_wr1, "HI", 3); 
	printf("Written %i Bytes\n",n);
	return 0;
}
