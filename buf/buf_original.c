#include <linux/init.h>
#include <linux/module.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/device.h>
#include <linux/types.h>
#include <linux/uaccess.h>
 
 
MODULE_LICENSE("GPL");
#define BUFFF_SIZE 32

static int driver_open(struct inode *geraetedatei, struct file *instanz);
static int driver_close(struct inode *geraetedatei, struct file *instanz);
static ssize_t driver_read(struct file *instanz,char *user,size_t count, loff_t *offset);
static ssize_t driver_write(struct file *instanz, const char *user, ssize_t count, loff_t *offs);
static int write_to_bufff(const char *user, int to_copy);
static int read_from_bufff(int to_copy, const char *user);

static dev_t template_dev_number;
static struct cdev *driver_object;
static struct file_operations fops = {
	.open = driver_open,
	.release = driver_close,
	.read = driver_read,
	.write=driver_write
};
static struct class * template_class;

static atomic_t write_count;

static wait_queue_head_t wq;

static char bufff[BUFFF_SIZE];
static int bufff_read = 0;
static int bufff_write = 0;
static int bufff_space = BUFFF_SIZE;

static int driver_open(struct inode *geraetedatei, struct file *instanz)
{
	printk(KERN_DEBUG "buf wird geoeffnet...");
	if(instanz->f_flags&O_RDWR || instanz->f_flags&O_WRONLY)
	{
		if(atomic_inc_and_test(&write_count) == 0) {
			printk(KERN_DEBUG "Sry Treiber gesperrt!");
			atomic_dec(&write_count);
			return -EBUSY;
		}
	}
	printk(KERN_DEBUG "buf Treiber erfolgreich geoeffnet");
	return 0;
}

static int driver_close(struct inode *geraetedatei, struct file *instanz)
{
	if(instanz->f_flags&O_RDWR || instanz->f_flags&O_WRONLY)
	{
		printk(KERN_DEBUG "buf Treiber wieder freigegeben!");
		atomic_dec(&write_count);
	}
	printk(KERN_DEBUG "buf closed");
	return 0;
}

static ssize_t driver_read(struct file *instanz,char *user,size_t count, loff_t *offset)
{
	int to_copy;
	to_copy = min((int)count, BUFFF_SIZE - bufff_space);
	int not_copied_byte = read_from_bufff(to_copy, user);
	printk(KERN_DEBUG "%d, %d", not_copied_byte, count);
	return to_copy - not_copied_byte;
}

static ssize_t driver_write(struct file *instanz, const char *user, ssize_t count, loff_t *offs)
{
	printk(KERN_DEBUG "buf write aufruf");
	printk(KERN_DEBUG "count: %d", count);
	
	int to_copy = (int)count;
	
	if(to_copy > bufff_space)
	{
		to_copy = bufff_space;
	}

	int not_copied_byte = write_to_bufff(user, to_copy);
	printk(KERN_DEBUG "not_copied_byte_write: %d", not_copied_byte);
	return to_copy - not_copied_byte;
}

static int __init ModInit(void)
{
	atomic_set(&write_count, -1);

        printk(KERN_ALERT "Hello, world\n");
	if(alloc_chrdev_region(&template_dev_number,0,2,"buf_treiber")<0)
	{
		printk(KERN_ALERT "Error reserving device number");
		return -EIO;
	}

	//init wait_queue
	init_waitqueue_head(&wq);
	
	driver_object = cdev_alloc();	
	if (driver_object ==NULL)
	{
		printk(KERN_ALERT "Error adding driver!");
		goto free_device_nmber;
	}
	driver_object->owner = THIS_MODULE;
	driver_object->ops = &fops;
	if (cdev_add(driver_object,template_dev_number,2))
	{
		printk(KERN_ALERT "Error adding Chardevice!");
		goto free_cdev;
	}
	//Eintrag im Sysfs
	template_class = class_create( THIS_MODULE, "buf_class");
	device_create(template_class,NULL,template_dev_number,NULL,"%s", "buf_device");
	printk(KERN_ALERT "buf wurde angemeldet mit Major %i und minor %i", MAJOR(template_dev_number), MINOR(template_dev_number));

        return 0;

	free_cdev:
		kobject_put( &driver_object->kobj);
	free_device_nmber:
		unregister_chrdev_region(template_dev_number,1);
		return -EIO;
}
 
static void __exit ModExit(void)
{
	device_destroy(template_class,template_dev_number);
	class_destroy(template_class);
	cdev_del(driver_object);
	unregister_chrdev_region(template_dev_number,1);
        printk(KERN_ALERT "Goodbye, cruel world\n");
	return;
}
 
static int write_to_bufff( const char *user,int to_copy)
{
	int bufff_rest = BUFFF_SIZE - bufff_write;
	int to_copy_tmp = to_copy;

	if(bufff_rest < to_copy) {
		to_copy_tmp = bufff_rest;	
	}

	if(wait_event_interruptible(wq,(bufff_space == 0)))
	{
		return -ERESTART;
	}

	unsigned long not_copied_byte = copy_from_user(bufff + bufff_write, user, to_copy_tmp);
	if(not_copied_byte != 0) {
		printk(KERN_ALERT "Did not copy all bytes!\n");
	}
	bufff_write += to_copy_tmp - not_copied_byte;
	bufff_space -= to_copy_tmp - not_copied_byte;

	if(bufff_write == BUFFF_SIZE) 
	{
		bufff_write = 0;
		not_copied_byte = copy_from_user(bufff + bufff_write, user + to_copy_tmp, to_copy - to_copy_tmp);
		if(not_copied_byte != 0) {
			printk(KERN_ALERT "Did not copy all bytes!\n");
		}
		bufff_write += (to_copy - to_copy_tmp) - not_copied_byte;
		bufff_space -= (to_copy - to_copy_tmp) - not_copied_byte;
	}
	return not_copied_byte;

}

static int read_from_bufff(int to_copy, const char *user)
{
	int bufff_rest = BUFFF_SIZE - bufff_read;
	int to_copy_tmp = to_copy;

	if(bufff_rest < to_copy) {
		to_copy_tmp = bufff_rest;	
	}

	unsigned long not_copied_byte = copy_to_user(user, bufff + bufff_read, to_copy_tmp);
	if(not_copied_byte != 0) {
		printk(KERN_ALERT "Did not copy all bytes!\n");
	}
	bufff_read += to_copy_tmp - (int)not_copied_byte;
	bufff_space += to_copy_tmp - (int)not_copied_byte;

	printk(KERN_DEBUG "bufff_read: %d, to_copy_tmp: %d", bufff_read, to_copy_tmp);
	if(bufff_read == BUFFF_SIZE) 
	{
		bufff_read = 0;
		not_copied_byte = copy_to_user(user + to_copy_tmp, bufff + bufff_read, to_copy - to_copy_tmp);
		if(not_copied_byte != 0) {
			printk(KERN_ALERT "Did not copy all bytes!\n");
		}
		bufff_read += (to_copy - to_copy_tmp) - (int)not_copied_byte;
		bufff_space += (to_copy - to_copy_tmp) - (int)not_copied_byte;
	
	}
	return (int)not_copied_byte;

}

module_init(ModInit);
module_exit(ModExit);
