#include <linux/init.h>
#include <linux/module.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/device.h>
#include <linux/types.h>
#include <linux/uaccess.h>
#include <linux/wait.h>
#include <linux/init.h>
#include <linux/module.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/device.h>
#include <linux/types.h>
#include <linux/uaccess.h>
#include <linux/wait.h>
#include <linux/sched.h>
 
MODULE_LICENSE("GPL");
#define BUFFF_SIZE 100

static int driver_open(struct inode *geraetedatei, struct file *instanz);
static int driver_close(struct inode *geraetedatei, struct file *instanz);
static ssize_t driver_read(struct file *instanz,char *user,size_t count, loff_t *offset);
static ssize_t driver_write(struct file *instanz, const char *user, ssize_t count, loff_t *offs);
static int write_to_bufff(int to_copy);
static int read_from_bufff(int to_copy);

static dev_t template_dev_number;
static struct cdev *driver_object;
static struct file_operations fops = {
	.open = driver_open,
	.release = driver_close,
	.read = driver_read,
	.write=driver_write
};
static struct class * template_class;

static atomic_t write_count;

static wait_queue_head_t wq;

static char bufff[BUFFF_SIZE];
static int bufff_read = 0;
static int bufff_write = 0;
static char tmp_bufff[BUFFF_SIZE];

static int driver_open(struct inode *geraetedatei, struct file *instanz)
{
	printk(KERN_DEBUG "buf wird geoeffnet...");
	if(instanz->f_flags&O_RDWR || instanz->f_flags&O_WRONLY)
	{
		if(atomic_inc_and_test(&write_count) == 0) {
			printk(KERN_DEBUG "Sry Treiber gesperrt!");
			atomic_dec(&write_count);
			return -EBUSY;
		}
	}
	printk(KERN_DEBUG "buf Treiber erfolgreich geoeffnet");
	return 0;
}

static int driver_close(struct inode *geraetedatei, struct file *instanz)
{
	if(instanz->f_flags&O_RDWR || instanz->f_flags&O_WRONLY)
	{
		printk(KERN_DEBUG "buf Treiber wieder freigegeben!");
		atomic_dec(&write_count);
	}
	printk(KERN_DEBUG "buf closed");
	return 0;
}

static ssize_t driver_read(struct file *instanz,char *user,size_t count, loff_t *offset)
{
	printk(KERN_INFO "treiber: driver_read ... ");
        
        if (count > BUFFF_SIZE)
                count = BUFFF_SIZE;
        
        count = read_from_bufff(count);
        
        if (copy_to_user(user, tmp_bufff, count) != 0)
        {
                printk("FAILED\n");
                printk(KERN_ALERT "\t-> treiber: driver_read copy_to_user failed\n");
                return -1;
        }
        
        printk("OK\n\t-> driver_read %d characters\n", count);
        
        return count ;
}

static ssize_t driver_write(struct file *instanz, const char __user *user, ssize_t count, loff_t *offs)
{
	int to_copy = count;
	printk(KERN_INFO "treiber: driver_write ... ");
        printk(KERN_DEBUG "count vor: %d", to_copy);
        if (to_copy > BUFFF_SIZE)
                to_copy = BUFFF_SIZE;
        if (copy_from_user(tmp_bufff, user, to_copy) != 0)
        {
                printk("FAILED\n");
                printk(KERN_ALERT "\t-> treiber: driver_write copy_from_user failed\n");
                return -1;
        }
        
        to_copy = write_to_bufff(to_copy);
                
        printk(KERN_DEBUG "count nach: %d", to_copy);
        printk(KERN_DEBUG "OK\n\t-> driver_write %d characters\n", to_copy);
        
        return to_copy;
}

static int __init ModInit(void)
{
	atomic_set(&write_count, -1);

        printk(KERN_ALERT "Hello, world\n");
	if(alloc_chrdev_region(&template_dev_number,0,2,"buf_treiber")<0)
	{
		printk(KERN_ALERT "Error reserving device number");
		return -EIO;
	}

	//init wait_queue
	init_waitqueue_head(&wq);
	
	driver_object = cdev_alloc();	
	if (driver_object ==NULL)
	{
		printk(KERN_ALERT "Error adding driver!");
		goto free_device_nmber;
	}
	driver_object->owner = THIS_MODULE;
	driver_object->ops = &fops;
	if (cdev_add(driver_object,template_dev_number,2))
	{
		printk(KERN_ALERT "Error adding Chardevice!");
		goto free_cdev;
	}
	//Eintrag im Sysfs
	template_class = class_create( THIS_MODULE, "buf_class");
	device_create(template_class,NULL,template_dev_number,NULL,"%s", "buf_device");
	printk(KERN_ALERT "buf wurde angemeldet mit Major %i und minor %i", MAJOR(template_dev_number), MINOR(template_dev_number));

        return 0;

	free_cdev:
		kobject_put( &driver_object->kobj);
	free_device_nmber:
		unregister_chrdev_region(template_dev_number,1);
		return -EIO;
}
 
static void __exit ModExit(void)
{
	device_destroy(template_class,template_dev_number);
	class_destroy(template_class);
	cdev_del(driver_object);
	unregister_chrdev_region(template_dev_number,1);
        printk(KERN_ALERT "Goodbye, cruel world\n");
	return;
}
 
static int write_to_bufff(int to_copy)
{
	int i;
        for (i = 0; i < to_copy; i++)
        {
                if ((bufff_write + 1) % BUFFF_SIZE == bufff_read) //kein Platz mehr !!
                {
                        if (wait_event_interruptible(wq, ((bufff_write + 1) % BUFFF_SIZE != bufff_read)))  
                                return -ERESTART;
                }
                bufff[bufff_write] = tmp_bufff[i];
                bufff_write = (bufff_write + 1) % BUFFF_SIZE;
        }
	printk(KERN_DEBUG "i: %d, buffwrite: %d", i, bufff_write);
        wake_up_interruptible(&wq);
	printk(KERN_DEBUG "i: %d", i);
        return i;
}

static int read_from_bufff(int to_copy)
{
	printk(KERN_DEBUG "to_copy anfang: %d", to_copy);
	int i;
        for (i = 0; i < to_copy; i++)
        {
		printk(KERN_DEBUG "read: %d, write: %d", bufff_read, bufff_write);
                if (bufff_read == bufff_write)                                          //  leer
                {
                        if (wait_event_interruptible(wq, (bufff_read != bufff_write)))  
                                return -ERESTART;
                }
                tmp_bufff[i] = bufff[bufff_read];
                bufff_read = (bufff_read + 1) % BUFFF_SIZE;
        }

	printk(KERN_DEBUG "i: %d", i);
        wake_up_interruptible(&wq);
        return i;
}

module_init(ModInit);
module_exit(ModExit);
