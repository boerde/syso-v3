#include <unistd.h>
#include <stdio.h>
#include <sys/sysinfo.h>

int main(int argc, char * args[])
{
  struct sysinfo kernelinfo;
  int retval;
  
  while(1)
  {
    printf("Hello in my Kernelspace\n");
    retval = sysinfo(&kernelinfo);
    if(retval != 0)
    {
      printf("Error by syscall\n");
    }
    else
    {
      printf("Homer ist running: %ld days %ld hours %ld minutes %ld seconds\n", kernelinfo.uptime/3600/24, kernelinfo.uptime/3600%24, kernelinfo.uptime/60%60, kernelinfo.uptime%60);
    }
    sleep(1);
  }
}

  
