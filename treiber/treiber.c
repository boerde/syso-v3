#include <linux/init.h>
#include <linux/module.h>
#include <linux/fs.h>
 
 
MODULE_LICENSE("GPL");

static int major;
static struct file_operations fops;
static int __init ModInit(void)
{
        printk(KERN_ALERT "Hello, world\n");
	if((major=register_chrdev(0,"mein_treiber",&fops)) == 0)
	{
		printk(KERN_ALERT "mein_treiber wurde nicht angemeldet");
		return -EIO;
	}
	
	printk(KERN_ALERT "mein_treiber wurde angemeldet");
        return 0;
}
 
static void __exit ModExit(void)
{
        printk(KERN_ALERT "Goodbye, cruel world\n");
	unregister_chrdev(major,"mein_treiber");
}
 
module_init(ModInit);
module_exit(ModExit);
