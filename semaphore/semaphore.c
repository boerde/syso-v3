#include <linux/init.h>
#include <linux/module.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/device.h>
#include <linux/semaphore.h>
#include <linux/delay.h>
 
MODULE_LICENSE("GPL");

static dev_t template_dev_number;
static struct cdev *driver_object;
static struct class * template_class;

static int write_count=0;

DEFINE_SEMAPHORE(semma);

//funktionen
static int driver_open(struct inode *geraetedatei, struct file *instanz);
static int driver_close(struct inode *geraetedatei, struct file *instanz);

static struct file_operations fops = {
	.open = driver_open,
	.release = driver_close,
};

static int driver_open(struct inode *geraetedatei, struct file *instanz)
{
	if(down_trylock(&semma) != 0) 
	{
		printk(KERN_DEBUG "Treiber schon offen!\n");
		while(1)
		{
			msleep(200);
			printk(KERN_DEBUG "Trying to get semaphore");
			if(down_trylock(&semma)==0)
				break;
		}
	}

	printk(KERN_DEBUG "GOT SEMMA\n");
	printk(KERN_DEBUG "Syso Treiber ist erfolgreich geoeffnet");
	msleep(5000);
	up(&semma);
	printk(KERN_DEBUG "Kritischer bereich verlassen\n");
	return 0;
}

static int driver_close(struct inode *geraetedatei, struct file *instanz)
{
	if(instanz->f_flags&O_RDWR || instanz->f_flags&O_WRONLY)
	{
		printk(KERN_DEBUG "Syso Treiber wieder freigegeben!");
		write_count--;
	}
	printk(KERN_DEBUG "Syso Treiber ist zu");
	return 0;
}

static int __init ModInit(void)
{
        printk(KERN_ALERT "Hello, world\n");
	//SEMAPHORE
	sema_init(&semma, 1);
	printk(KERN_DEBUG "Semaphore initialised\n");

	if(alloc_chrdev_region(&template_dev_number,0,1,"semaphore_treiber")<0)
	{
		printk(KERN_ALERT "Error reserving device number");
		return -EIO;
	}
	driver_object = cdev_alloc();	
	if (driver_object ==NULL)
	{
		printk(KERN_ALERT "Error adding driver!");
		goto free_device_nmber;
	}
	driver_object->owner = THIS_MODULE;
	driver_object->ops = &fops;
	if (cdev_add(driver_object,template_dev_number,1))
	{
		printk(KERN_ALERT "Error registering driver!");
		goto free_cdev;
	}
	//Eintrag im Sysfs
	template_class = class_create( THIS_MODULE, "semaphore_class");
	device_create(template_class,NULL,template_dev_number,NULL,"%s", "semaphore_device");
	printk(KERN_ALERT "semaphore_treiber wurde angemeldet mit Major %i und minor %i", MAJOR(template_dev_number), MINOR(template_dev_number));
        return 0;

	free_cdev:
		kobject_put( &driver_object->kobj);
	free_device_nmber:
		unregister_chrdev_region(template_dev_number,1);
		return -EIO;
}
 
static void __exit ModExit(void)
{
	device_destroy(template_class,template_dev_number);
	class_destroy(template_class);
	cdev_del(driver_object);
	unregister_chrdev_region(template_dev_number,1);
        printk(KERN_ALERT "Goodbye, cruel world\n");
	return;
}
 
module_init(ModInit);
module_exit(ModExit);
