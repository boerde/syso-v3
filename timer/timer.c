#include <linux/init.h>
#include <linux/module.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/device.h>
#include <linux/types.h>
#include <linux/uaccess.h>
#include <linux/timer.h>
 
 
MODULE_LICENSE("GPL");

static int driver_open(struct inode *geraetedatei, struct file *instanz);
static int driver_close(struct inode *geraetedatei, struct file *instanz);
static ssize_t driver_read(struct file *instanz,char *user,size_t count, loff_t *offset);
static ssize_t driver_write(struct file *instanz, const char *user, ssize_t count, loff_t *offs);

static dev_t template_dev_number;
static struct cdev *driver_object;
static struct file_operations fops = {
	.open = driver_open,
	.release = driver_close,
	.read = driver_read,
	.write=driver_write
};
static struct class * template_class;

static atomic_t write_count;
static char timer_str[] = "Hello World!";

//timer
static struct timer_list mytimer;

static void inc_count(unsigned long arg)
{
	printk("\ninc_count called (%ld)...\n", mytimer.expires);
	mytimer.expires=jiffies+(2*HZ);
	add_timer(&mytimer);
}

static int driver_open(struct inode *geraetedatei, struct file *instanz)
{
	printk(KERN_DEBUG "timer wird geoeffnet...");
	if(instanz->f_flags&O_RDWR || instanz->f_flags&O_WRONLY)
	{
		if(atomic_inc_and_test(&write_count) == 0) {
			printk(KERN_DEBUG "Sry Treiber gesperrt!");
			atomic_dec(&write_count);
			return -EBUSY;
		}
	}
	printk(KERN_DEBUG "Syso Treiber erfolgreich geoeffnet");
	return 0;
}

static int driver_close(struct inode *geraetedatei, struct file *instanz)
{
	if(instanz->f_flags&O_RDWR || instanz->f_flags&O_WRONLY)
	{
		printk(KERN_DEBUG "Syso Treiber wieder freigegeben!");
		atomic_dec(&write_count);
	}
	printk(KERN_DEBUG "timer closed");
	return 0;
}

static ssize_t driver_read(struct file *instanz,char *user,size_t count, loff_t *offset)
{
	unsigned long not_copied_byte, to_copy;
	to_copy = min(count, strlen(timer_str)+1);
	//printk(KERN_DEBUG "timer read aufruf");
	not_copied_byte = copy_to_user(user, timer_str, to_copy); 
	return to_copy-not_copied_byte;
}

static ssize_t driver_write(struct file *instanz, const char *user, ssize_t count, loff_t *offs)
{
	printk("timer write aufruf");
	printk("count: %d", count);
	return count;
}

static int __init ModInit(void)
{
	atomic_set(&write_count, -1);
        printk(KERN_ALERT "Hello, world\n");
	if(alloc_chrdev_region(&template_dev_number,0,1,"timer_treiber")<0)
	{
		printk(KERN_ALERT "Error reserving device number");
		return -EIO;
	}
	driver_object = cdev_alloc();	
	if (driver_object ==NULL)
	{
		printk(KERN_ALERT "Error adding driver!");
		goto free_device_nmber;
	}
	driver_object->owner = THIS_MODULE;
	driver_object->ops = &fops;
	if (cdev_add(driver_object,template_dev_number,1))
	{
		printk(KERN_ALERT "Error registering driver!");
		goto free_cdev;
	}

	//Init Timer
	//
	init_timer(&mytimer);
	mytimer.function=inc_count;
	mytimer.data = 0;
	mytimer.expires = jiffies + (2*HZ); //2 seconds
	add_timer(&mytimer);

	//Eintrag im Sysfs
	template_class = class_create( THIS_MODULE, "timer_class");
	device_create(template_class,NULL,template_dev_number,NULL,"%s", "timer_device");
	printk(KERN_ALERT "timer wurde angemeldet mit Major %i und minor %i", MAJOR(template_dev_number), MINOR(template_dev_number));
        return 0;

	free_cdev:
		kobject_put( &driver_object->kobj);
	free_device_nmber:
		unregister_chrdev_region(template_dev_number,1);
		return -EIO;
}
 
static void __exit ModExit(void)
{
	device_destroy(template_class,template_dev_number);
	class_destroy(template_class);
	cdev_del(driver_object);
	unregister_chrdev_region(template_dev_number,1);
        printk(KERN_ALERT "Goodbye, cruel world\n");
	if(del_timer_sync(&mytimer))
		printk("Aktiver Timer deaktiviert\n");
	return;
}
 
module_init(ModInit);
module_exit(ModExit);
