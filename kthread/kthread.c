#include <linux/init.h>
#include <linux/module.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/device.h>
#include <linux/types.h>
#include <linux/uaccess.h>
#include <linux/kthread.h>
#include <linux/interrupt.h>
#include <linux/kthread.h>
#include <linux/interrupt.h>
  
MODULE_LICENSE("GPL");

static int driver_open(struct inode *geraetedatei, struct file *instanz);
static int driver_close(struct inode *geraetedatei, struct file *instanz);
static ssize_t driver_read(struct file *instanz,char *user,size_t count, loff_t *offset);
static ssize_t driver_write(struct file *instanz, const char *user, ssize_t count, loff_t *offs);

static dev_t template_dev_number;
static struct cdev *driver_object;
static struct file_operations fops = {
	.open = driver_open,
	.release = driver_close,
	.read = driver_read,
	.write=driver_write
};
static struct class * template_class;

static atomic_t write_count;
static char kthread_str[] = "Hello World!";

//KTHREAD INIT
//
static struct task_struct *thread_id;
static wait_queue_head_t wq;
static DECLARE_COMPLETION(on_exit);

static int thread_code(void *data)
{
	unsigned long timeout;
	int i;
	allow_signal(SIGTERM);
	while(1)
	{
		timeout=2*HZ;
		timeout=wait_event_interruptible_timeout(wq,(timeout==0),timeout);
		printk("Thread_funktion: Wake up... %ld\n", timeout);
		if(timeout == -ERESTARTSYS) {
			printk("got_signal. break\n");
			break;
		}
	}
	complete_and_exit(&on_exit,0);
}

static int driver_open(struct inode *geraetedatei, struct file *instanz)
{
	printk(KERN_DEBUG "kthread wird geoeffnet...");
	if(instanz->f_flags&O_RDWR || instanz->f_flags&O_WRONLY)
	{
		if(atomic_inc_and_test(&write_count) == 0) {
			printk(KERN_DEBUG "Sry Treiber gesperrt!");
			atomic_dec(&write_count);
			return -EBUSY;
		}
	}
	printk(KERN_DEBUG "Syso Treiber erfolgreich geoeffnet");
	return 0;
}

static int driver_close(struct inode *geraetedatei, struct file *instanz)
{
	if(instanz->f_flags&O_RDWR || instanz->f_flags&O_WRONLY)
	{
		printk(KERN_DEBUG "Syso Treiber wieder freigegeben!");
		atomic_dec(&write_count);
	}
	printk(KERN_DEBUG "kthread closed");
	return 0;
}

static ssize_t driver_read(struct file *instanz,char *user,size_t count, loff_t *offset)
{
	unsigned long not_copied_byte, to_copy;
	to_copy = min(count, strlen(kthread_str)+1);
//	printk(KERN_DEBUG "kthread read aufruf");
	not_copied_byte = copy_to_user(user, kthread_str, to_copy); 
	return to_copy-not_copied_byte;
}

static ssize_t driver_write(struct file *instanz, const char *user, ssize_t count, loff_t *offs)
{
	printk("kthread write aufruf");
	printk("count: %d", count);
	return count;
}

static int __init ModInit(void)
{
	atomic_set(&write_count, -1);
        printk(KERN_ALERT "Hello, world\n");
	if(alloc_chrdev_region(&template_dev_number,0,1,"kthread_treiber")<0)
	{
		printk(KERN_ALERT "Error reserving device number");
		return -EIO;
	}
	driver_object = cdev_alloc();	
	if (driver_object ==NULL)
	{
		printk(KERN_ALERT "Error adding driver!");
		goto free_device_nmber;
	}
	driver_object->owner = THIS_MODULE;
	driver_object->ops = &fops;
	if (cdev_add(driver_object,template_dev_number,1))
	{
		printk(KERN_ALERT "Error registering driver!");
		goto free_cdev;
	}

	//INIT WQ
	//
	//
	init_waitqueue_head(&wq);
	thread_id = kthread_create(thread_code,NULL,"mykthread");
	if(thread_id == 0) 
	{
		return -EIO;
	}
	wake_up_process(thread_id);
	
	//Eintrag im Sysfs
	template_class = class_create( THIS_MODULE, "kthread_class");
	device_create(template_class,NULL,template_dev_number,NULL,"%s", "kthread_device");
	printk(KERN_ALERT "kthread wurde angemeldet mit Major %i und minor %i", MAJOR(template_dev_number), MINOR(template_dev_number));
        return 0;

	free_cdev:
		kobject_put( &driver_object->kobj);
	free_device_nmber:
		unregister_chrdev_region(template_dev_number,1);
		return -EIO;
}
 
static void __exit ModExit(void)
{

	//Kill thread
	//
	kill_pid( task_pid(thread_id), SIGTERM,1);
	wait_for_completion(&on_exit);
	device_destroy(template_class,template_dev_number);
	class_destroy(template_class);
	cdev_del(driver_object);
	unregister_chrdev_region(template_dev_number,1);
        printk(KERN_ALERT "Goodbye, cruel world\n");


	return;
}
 
module_init(ModInit);
module_exit(ModExit);
