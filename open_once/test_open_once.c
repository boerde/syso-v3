#include <stdio.h>
#include <fcntl.h>
#include <stdarg.h>
#include <unistd.h>
#include <stdlib.h>

int main(int argc, char * args[])
{
	int fd_wr1, fd_wr2;
	int pid;
	pid = fork();
	if(pid==0)
	{
		if((fd_wr1 = open("/dev/mutex_device", O_RDWR)) < 0) 
		{
			printf("fd_wr1: konnte Datei nicht oeffnen\n");
		}
		else
		{
			printf("fd_wr1: Datei zum Lesen und Schreiben geöffnet\n");
		}
	}
	else if(pid > 0)
	{
		if((fd_wr2 = open("/dev/mutex_device", O_RDWR)) < 0) 
		{
			printf("fd_wr2: konnte Datei nicht oeffnen\n");
		}
		else
		{
			printf("fd_wr2: Datei zum Lesen und Schreiben geöffnet\n");
		}
	}
	else
	{
		printf("Fehler bei fork!\n");
	}

	return 0;
}
