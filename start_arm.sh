#!/bin/bash

#long qemu command starting:
# - Networkcard
# - virtual console
# - telnet server

qemu-system-arm -kernel output/images/zImage -M versatilepb -m 128M -initrd ./output/images/rootfs.ext2 -serial stdio -net nic,model=rtl8139,macaddr=00:00:00:00:00:25 -append "ramdisk_size=10000" -net tap,ifname=tap9,vlan=9,script=no,downscript=no

