#include <linux/init.h>
#include <linux/module.h>
#include <linux/fs.h>
 
 
MODULE_LICENSE("GPL");

static int write_count=0;

static int driver_open(struct inode *geraetedatei, struct file *instanz)
{
	if(instanz->f_flags&O_RDWR || instanz->f_flags&O_WRONLY)
	{
		if(write_count > 0) 
		{
			printk(KERN_DEBUG "Sry Treiber gesperrt!");
			return -EBUSY;
		}
		write_count++;
	}
	printk(KERN_DEBUG "Syso Treiber ist erfolgreich geoeffnet");
	return 0;
}

static int driver_close(struct inode *geraetedatei, struct file *instanz)
{
	if(instanz->f_flags&O_RDWR || instanz->f_flags&O_WRONLY)
	{
		printk(KERN_DEBUG "Syso Treiber wieder freigegeben!");
		write_count--;
	}
	printk(KERN_DEBUG "Syso Treiber ist zu");
	return 0;
}
static int __init ModInit(void)
{
        printk(KERN_ALERT "Hello, world\n");
        return 0;
}
 
static void __exit ModExit(void)
{
        printk(KERN_ALERT "Goodbye, cruel world\n");
}
 
module_init(ModInit);
module_exit(ModExit);
