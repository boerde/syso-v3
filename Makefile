CROSS_COMPILE = arm-linux-
CC = ./buildroot-2012.08/output/host/usr/bin/$(CROSS_COMPILE)gcc
CFLAGS = -Wall

all: startc startc_static 
startc: startc.c
	$(CC) $(CFLAGS) startc.c -o $@
 
startc_static: startc.c
	$(CC) -static $(CFLAGS) startc.c -o $@
