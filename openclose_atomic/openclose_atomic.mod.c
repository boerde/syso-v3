#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
 .name = KBUILD_MODNAME,
 .init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
 .exit = cleanup_module,
#endif
 .arch = MODULE_ARCH_INIT,
};

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0xd6ce8b7c, "module_layout" },
	{ 0x9157726, "cdev_del" },
	{ 0xe5e6e9c5, "class_destroy" },
	{ 0x260285e3, "device_destroy" },
	{ 0x7485e15e, "unregister_chrdev_region" },
	{ 0x22c828d5, "device_create" },
	{ 0x8bc4ea44, "__class_create" },
	{ 0xab9ed595, "kobject_put" },
	{ 0x2ce09f33, "cdev_add" },
	{ 0x2be69c12, "cdev_alloc" },
	{ 0x29537c9e, "alloc_chrdev_region" },
	{ 0xefd6cf06, "__aeabi_unwind_cpp_pr0" },
	{ 0x2e5810c6, "__aeabi_unwind_cpp_pr1" },
	{ 0x27e1a049, "printk" },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=";

